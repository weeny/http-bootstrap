<?php

namespace Weeny\Core\HttpBootstrap;

require_once 'vendor/autoload.php';

use Weeny\Core\ContainerLoader\ContainerLoader;
use Weeny\Core\HttpBootstrap\Exceptions\ServerErrorException;
use Weeny\Core\PackageManager\DependencyResolver;
use Weeny\Lib\CircularWatcher\CircularReferenceWatcher;
use Zend\HttpHandlerRunner\Emitter\SapiEmitter;

try {

    if ( !file_exists('packages.php') ) {
        throw new ServerErrorException('Weeny configuration error. Code: #1');
    }

    $packages = require 'packages.php';

    $handlerLoader = new DependencyResolverHandlerLoader(
        new HandlerLoader(
            new ContainerLoader()
        ),
        new DependencyResolver(
            new CircularReferenceWatcher()
        )
    );

    $handler = $handlerLoader->loadHandler(...$packages);
    $handler->execute();

} catch (\Exception $e) {
    $exceptionConverter = new ExceptionToResponseConverter();
    $response = $exceptionConverter->convertToResponse($e);
    (new SapiEmitter())->emit($response);
}


?>