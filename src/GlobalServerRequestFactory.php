<?php

namespace Weeny\Core\HttpBootstrap;

use Psr\Http\Message\ServerRequestInterface;
use Weeny\Core\HttpBootstrap\Contract\GlobalServerRequestFactoryInterface;
use Zend\Diactoros\ServerRequestFactory;

class GlobalServerRequestFactory implements GlobalServerRequestFactoryInterface
{

    /**
     * @inheritDoc
     */
    public function getFromGlobal(): ServerRequestInterface
    {
        return ServerRequestFactory::fromGlobals();
    }
}