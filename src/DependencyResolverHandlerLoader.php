<?php

namespace Weeny\Core\HttpBootstrap;

use Weeny\Contract\Package\DependencyResolverInterface;
use Weeny\Contract\Package\PackageInterface;
use Weeny\Core\HttpBootstrap\Contract\GlobalHttpHandlerInterface;
use Weeny\Core\HttpBootstrap\Contract\MainHandlerLoaderInterface;

class DependencyResolverHandlerLoader implements MainHandlerLoaderInterface
{

    /**
     * @var MainHandlerLoaderInterface
     */
    private $handlerLoader;

    /**
     * @var DependencyResolverInterface
     */
    private $dependencyResolver;

    public function __construct(
        MainHandlerLoaderInterface $handlerLoader,
        DependencyResolverInterface $dependencyResolver
    )
    {
        $this->handlerLoader = $handlerLoader;
        $this->dependencyResolver = $dependencyResolver;
    }

    /**
     * @inheritDoc
     */
    public function loadHandler(PackageInterface ...$packages): GlobalHttpHandlerInterface
    {
        $packages = $this->dependencyResolver->resolveDependencies(...$packages);
        return $this->handlerLoader->loadHandler(...$packages->toArray());
    }
}