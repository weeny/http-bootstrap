<?php

namespace Weeny\Core\HttpBootstrap\Contract;

use Psr\Http\Message\ServerRequestInterface;

interface GlobalServerRequestFactoryInterface
{

    /**
     * Returning server request instanse creating from global enviroments
     * @return ServerRequestInterface
     */
    public function getFromGlobal(): ServerRequestInterface;
}