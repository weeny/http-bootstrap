<?php

namespace Weeny\Core\HttpBootstrap\Contract;

use Psr\Container\ContainerInterface;

interface GlobalHttpHandlerInterface
{
    /**
     * Return loaded ServiceContainer
     * @return ContainerInterface
     */
    public function getContainer(): ContainerInterface;

    /**
     * Execute http request from global environment and send http response.
     * @return mixed
     */
    public function execute();

}