<?php

namespace Weeny\Core\HttpBootstrap\Contract;

use Weeny\Contract\Package\PackageInterface;

interface MainHandlerLoaderInterface
{

    /**
     * Are Initializing container with configuration from packages
     * and are initializing global http handler instance
     * with this container
     *
     * @return GlobalHttpHandlerInterface
     */
    public function loadHandler(PackageInterface ... $packages): GlobalHttpHandlerInterface;
}