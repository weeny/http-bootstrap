<?php

namespace Weeny\Core\HttpBootstrap;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Weeny\Contract\Container\ContainerLoaderInterface;
use Weeny\Contract\Http\ExceptionToResponseConverterInterface;
use Weeny\Core\HttpBootstrap\Contract\GlobalHttpHandlerInterface;
use Weeny\Core\HttpBootstrap\Contract\GlobalServerRequestFactoryInterface;
use Weeny\Core\HttpBootstrap\Exceptions\ServerErrorException;
use Zend\HttpHandlerRunner\Emitter\EmitterInterface;
use Zend\HttpHandlerRunner\Emitter\SapiEmitter;

class GlobalHttpHandler implements GlobalHttpHandlerInterface, LoggerAwareInterface
{

    use LoggerAwareTrait;

    /**
     * @var ContainerLoaderInterface
     */
    private $container;

    private $isResponseEmitted;

    public function __construct(
        ContainerInterface $container
    )
    {
        $this->container = $container;
        $this->setLogger(new NullLogger());

        if ( $this->container->has(LoggerInterface::class) ) {
            $logger = $this->container->get(LoggerInterface::class);
            if ($logger instanceof LoggerInterface) {
                $this->setLogger($logger);
            }
        }
    }

    public function getContainer(): ContainerInterface {
        return $this->container;
    }

    public function execute() {
        register_shutdown_function([$this, 'shutdown']);
        ob_start();

        try {
            $handler = $this->getRequestHandler();
            $request = $this->getRequestFactory()->getFromGlobal();
            $response = $handler->handle($request);
        } catch (\Throwable $e) {
            $response = $this->getResponseFromException($e);
        }

        $contents = ob_get_contents();
        ob_end_clean();

        $this->getEmitter()->emit($response);
        $this->isResponseEmitted = true;

        if ( !empty($contents) ) {
            $this->logger->notice('The output buffer contents: "'.$contents.'"');
        }
    }

    private function getRequestHandler(): RequestHandlerInterface {
        if ( !$this->container->has(RequestHandlerInterface::class) ) {
            $message = sprintf('Not found service "%s" in container.', RequestHandlerInterface::class);
            $this->logger->emergency($message);
            throw new ServerErrorException($message);
        }

        $handler = $this->container->get(RequestHandlerInterface::class);

        if ( !($handler instanceof RequestHandlerInterface) ) {
            $message = sprintf('Service "%s" from container does\'t implements that interface.', RequestHandlerInterface::class);
            $this->logger->emergency($message);
            throw new ServerErrorException($message);
        }

        return $handler;
    }

    private function getRequestFactory(): GlobalServerRequestFactoryInterface {
        return $this->getService(
            GlobalServerRequestFactoryInterface::class,
            GlobalServerRequestFactory::class
        );
    }

    private function getEmitter(): EmitterInterface {
        return $this->getService(
            EmitterInterface::class,
            SapiEmitter::class
        );
    }

    private function getService(string $interfaceName, string $defaultClassName) {
        if ( $this->container->has($interfaceName) ) {
            $service = $this->container->get($interfaceName);

            if ($service instanceof $interfaceName) {
                return $service;
            }

            $this->logger->error(sprintf(
                'Service "%s" from container does\'t implements that interface.',
                $interfaceName
            ));
        }

        return new $defaultClassName();
    }

    private function getResponseFromException(\Throwable $throwable): ResponseInterface {
        return $this->getExceptionConverter()->convertToResponse($throwable);
    }

    private function getExceptionConverter(): ExceptionToResponseConverterInterface {
        return $this->getService(
            ExceptionToResponseConverterInterface::class,
            ExceptionToResponseConverter::class
        );
    }

    public function shutdown() {
        if ( $this->isResponseEmitted ) {
            return;
        }

        $this->logger->emergency('Execution is interrupted!!');
    }
}