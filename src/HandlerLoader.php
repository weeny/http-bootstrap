<?php

namespace Weeny\Core\HttpBootstrap;

use Weeny\Contract\Container\ContainerLoaderInterface;
use Weeny\Contract\Package\PackageInterface;
use Weeny\Core\HttpBootstrap\Contract\GlobalHttpHandlerInterface;
use Weeny\Core\HttpBootstrap\Contract\MainHandlerLoaderInterface;
use Weeny\Core\PackageManager\Collection\PackageCollection;

class HandlerLoader implements MainHandlerLoaderInterface
{

    /**
     * @var ContainerLoaderInterface
     */
    protected $containerLoader;

    public function __construct(
        ContainerLoaderInterface $containerLoader = null
    )
    {
        $this->containerLoader = $containerLoader;
    }

    /**
     * @inheritDoc
     */
    public function loadHandler(PackageInterface ... $packages): GlobalHttpHandlerInterface {
        $packageCollection = new PackageCollection(...$packages);
        $container = $this->containerLoader->loadFromPackagesConfiguration($packageCollection);
        return new GlobalHttpHandler($container);
    }

}