<?php

namespace Weeny\Core\HttpBootstrap\Exceptions;

class ServerErrorException extends HttpException
{
    /**
     * @inheritDoc
     */
    public function getHttpCode(): int
    {
        return 500;
    }
}