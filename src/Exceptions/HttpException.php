<?php

namespace Weeny\Core\HttpBootstrap\Exceptions;

use Weeny\Contract\Http\Exceptions\HttpExceptionInterface;

class HttpException extends \Exception implements HttpExceptionInterface
{

    /**
     * @inheritDoc
     */
    public function getHttpCode(): int
    {
        return $this->getCode();
    }
}