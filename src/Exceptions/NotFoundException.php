<?php

namespace Weeny\Core\HttpBootstrap\Exceptions;

class NotFoundException extends HttpException
{

    /**
     * @inheritDoc
     */
    public function getHttpCode(): int
    {
        return 404;
    }
}