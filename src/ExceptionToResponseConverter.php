<?php

namespace Weeny\Core\HttpBootstrap;

use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Weeny\Contract\Http\Exceptions\HttpExceptionInterface;
use Weeny\Contract\Http\ExceptionToResponseConverterInterface;
use Zend\Diactoros\ResponseFactory;

class ExceptionToResponseConverter implements ExceptionToResponseConverterInterface
{

    /**
     * @var ResponseFactoryInterface
     */
    protected $responseFactory;

    public function __construct(
        ResponseFactoryInterface $responseFactory = null
    )
    {
        $this->responseFactory = $responseFactory ?? new ResponseFactory();
    }

    /**
     * @inheritDoc
     */
    public function convertToResponse(\Throwable $exception): ResponseInterface
    {
        if ( $exception instanceof HttpExceptionInterface ) {
            return $this->responseFactory->createResponse($exception->getHttpCode());
        }

        return $this->responseFactory->createResponse(500);
    }
}