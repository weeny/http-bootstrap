<?php

namespace Weeny\Core\HttpBootstrap\Tests;

use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Weeny\Contract\Http\ExceptionToResponseConverterInterface;
use Weeny\Core\HttpBootstrap\GlobalHttpHandler;
use Zend\HttpHandlerRunner\Emitter\EmitterInterface;

class GlobalHttpHandlerTest extends TestCase
{

    /**
     * @var \PHPUnit\Framework\MockObject\MockObject|ContainerInterface
     */
    private $container;

    public function setUp()
    {
        parent::setUp();
        $this->container = $this->getMockBuilder(ContainerInterface::class)->getMock();
    }

    public function testWithoutRequestHandlerInContainer() {
        $emitter = $this->getMockBuilder(EmitterInterface::class)->getMock();
        $emitter->method('emit')->willReturnCallback(function (ResponseInterface $response) {
            $this->assertEquals(500, $response->getStatusCode());
            return true;
        });

        $this->container->method('has')->willReturnMap([
            [RequestHandlerInterface::class, false],
            [ExceptionToResponseConverterInterface::class, false],
            [EmitterInterface::class, true],
        ]);

        $this->container->method('get')->willReturnMap([
            [EmitterInterface::class, $emitter],
        ]);


        $handler = new GlobalHttpHandler($this->container);
        $handler->execute();
    }

    public function testWithIncorrectRequestHandlerInContainer() {
        $emitter = $this->getMockBuilder(EmitterInterface::class)->getMock();
        $emitter->method('emit')->willReturnCallback(function (ResponseInterface $response) {
            $this->assertEquals(500, $response->getStatusCode());
            return true;
        });

        $this->container->method('has')->willReturnMap([
            [RequestHandlerInterface::class, true],
            [ExceptionToResponseConverterInterface::class, false],
            [EmitterInterface::class, true],
        ]);

        $this->container->method('get')->willReturnMap([
            [EmitterInterface::class, $emitter],
            [RequestHandlerInterface::class, new \stdClass()],
        ]);


        $handler = new GlobalHttpHandler($this->container);
        $handler->execute();
    }

    public function testPositive() {
        $response = $this->getMockBuilder(ResponseInterface::class)->getMock();
        $requestHandler = new class($response) implements RequestHandlerInterface {
            private $response;
            public $request;
            public function __construct($response)
            {
                $this->response = $response;
            }

            public function handle(ServerRequestInterface $request): ResponseInterface
            {
                $this->request = $request;
                return $this->response;
            }
        };
        $emitter = $this->getMockBuilder(EmitterInterface::class)->getMock();
        $emitter->method('emit')->with($response);

        $this->container->method('has')->willReturnMap([
            [RequestHandlerInterface::class, true],
            [ExceptionToResponseConverterInterface::class, false],
            [EmitterInterface::class, true],
        ]);

        $this->container->method('get')->willReturnMap([
            [EmitterInterface::class, $emitter],
            [RequestHandlerInterface::class, $requestHandler],
        ]);


        $handler = new GlobalHttpHandler($this->container);
        $handler->execute();
        $this->assertInstanceOf(ServerRequestInterface::class, $requestHandler->request);
    }
}