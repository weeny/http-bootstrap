<?php

namespace Weeny\Core\HttpBootstrap\Tests;

use PHPUnit\Framework\TestCase;
use Weeny\Core\HttpBootstrap\Exceptions\HttpException;
use Weeny\Core\HttpBootstrap\Exceptions\NotFoundException;
use Weeny\Core\HttpBootstrap\ExceptionToResponseConverter;

class ExceptionToResponseConverterTest extends TestCase
{

    /**
     * @dataProvider dataProviderForConverter
     */
    public function testConvert(\Throwable $e, $expectedCode) {
        $converter = new ExceptionToResponseConverter();
        $response = $converter->convertToResponse($e);
        $this->assertEquals($expectedCode, $response->getStatusCode());
    }

    public function dataProviderForConverter() {
        return [
            [new \Exception(), 500],
            [new NotFoundException(), 404],
            [new HttpException('', 502), 502]
        ];
    }
}