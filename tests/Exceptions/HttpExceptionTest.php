<?php

namespace Weeny\Core\HttpBootstrap\Tests;

use PHPUnit\Framework\TestCase;
use Weeny\Core\HttpBootstrap\Exceptions\HttpException;
use Weeny\Core\HttpBootstrap\Exceptions\NotFoundException;
use Weeny\Core\HttpBootstrap\Exceptions\ServerErrorException;

class HttpExceptionTest extends TestCase
{

    /**
     * @dataProvider dataProviderForHttpException
     */
    public function testHttpException($message, $code, $expectedCode) {
        $exeption = new HttpException($message, $code);
        $this->assertEquals($expectedCode, $exeption->getHttpCode());
    }

    public function dataProviderForHttpException() {
        return [
            ['Some message one', 0, 0],
            ['Some message two', 400, 400],
        ];
    }

    /**
     * @dataProvider dataProviderForNotFoundException
     */
    public function testNotFoundException($message, $code, $expectedCode) {
        $exeption = new NotFoundException($message, $code);
        $this->assertEquals($expectedCode, $exeption->getHttpCode());
    }

    public function dataProviderForNotFoundException() {
        return [
            ['Some message one', 0, 404],
            ['Some message two', 400, 404],
        ];
    }

    /**
     * @dataProvider dataProviderForServerErrorException
     */
    public function testServerErrorException($message, $code, $expectedCode) {
        $exeption = new ServerErrorException($message, $code);
        $this->assertEquals($expectedCode, $exeption->getHttpCode());
    }

    public function dataProviderForServerErrorException() {
        return [
            ['Some message one', 0, 500],
            ['Some message two', 400, 500],
        ];
    }
}