<?php

namespace Weeny\Core\HttpBootstrap\Tests;

use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use Weeny\Contract\Container\ContainerLoaderInterface;
use Weeny\Contract\Package\DependencyResolverInterface;
use Weeny\Core\HttpBootstrap\DependencyResolverHandlerLoader;
use Weeny\Core\HttpBootstrap\HandlerLoader;
use Weeny\Core\PackageManager\Collection\PackageCollection;
use Weeny\Core\PackageManager\Tests\Fixture\DependencedPackageOne;
use Weeny\Core\PackageManager\Tests\Fixture\SimplePackageOne;
use Weeny\Core\PackageManager\Tests\Fixture\SimplePackageTwo;

class HandlerLoaderTest extends TestCase
{

    public function testLoad() {
        $packages = [
            new SimplePackageOne(),
            new SimplePackageTwo()
        ];

        $collection = new PackageCollection(
            new SimplePackageOne(),
            new SimplePackageTwo(),
            new DependencedPackageOne()
        );

        $container = $this->getMockBuilder(ContainerInterface::class)->getMock();

        $dependencyLoader = $this->getMockBuilder(DependencyResolverInterface::class)->getMock();

        $dependencyLoader
            ->expects($this->once())
            ->method('resolveDependencies')
            ->with(...$packages)
            ->willReturn($collection);

        $containerLoader = $this->getMockBuilder(ContainerLoaderInterface::class)->getMock();
        $containerLoader
            ->expects($this->once())
            ->method('loadFromPackagesConfiguration')
            ->with($collection)
            ->willReturn($container);

        $loader = new DependencyResolverHandlerLoader(
            new HandlerLoader($containerLoader),
            $dependencyLoader
        );

        $handler = $loader->loadHandler(...$packages);
        $this->assertEquals($container, $handler->getContainer());
    }
}